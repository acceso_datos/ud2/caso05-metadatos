package aplicacion;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSetMetaData;

/**
 *
 * Api info:
 *         https://docs.oracle.com/en/java/javase/17/docs/api/java.sql/java/sql/DatabaseMetaData.html
 *         https://docs.oracle.com/en/java/javase/17/docs/api/java.sql/java/sql/ResultSetMetaData.html
 * 
 */
public class App {
	static Connection con;

	public static void main(String[] args) {

		try {
			con = ConexionBD.getConexion();
			if (con != null) {
				verMetaDatos();
				verDatosTabla();

//				ejercicio1();
//				ejercicio2();
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				ConexionBD.cerrar();
			} catch (SQLException e) {
				System.err.println(e.getMessage());
			}
		}

	}

	private static void verMetaDatos() {
		try {
			DatabaseMetaData dbmd = con.getMetaData();
			// Informacion del producto de la base de datos
			infoProducto(dbmd);
			// Informacion del driver JDBC
			infoVersiones(dbmd);
			// Informacion sobre las funciones de la base de datos
			infoFunciones(dbmd);
			// Tablas existentes
			infoTablas(dbmd);
			// Procedimientos existentes
			infoProcedimientos(dbmd);
			// Bases de datos disponibles
			infoBD(dbmd);
		} catch (SQLException se) {
			System.err.println(se);
		}
	}

	private static void infoProducto(DatabaseMetaData dbmd) throws SQLException {
		System.out.println(">>>Informacion sobre el SGBD:");
		String producto = dbmd.getDriverName();
		String version = dbmd.getDatabaseProductVersion();
		boolean soportaSQL = dbmd.supportsANSI92EntryLevelSQL();
		boolean soportaConvert = dbmd.supportsConvert();
		boolean usaFich = dbmd.usesLocalFiles();
		boolean soportaGRBY = dbmd.supportsGroupBy();
		boolean soportaMinSQL = dbmd.supportsMinimumSQLGrammar();
		boolean soportaTransac = dbmd.supportsTransactions();
		boolean soportaBatch = dbmd.supportsBatchUpdates();
		boolean soportaRsSensible = dbmd.supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);
		boolean soportaRsNoSensible = dbmd.supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE);
		boolean soportaRsForward = dbmd.supportsResultSetType(ResultSet.TYPE_FORWARD_ONLY);

		String nombre = dbmd.getUserName();
		String url = dbmd.getURL();
		System.out.println(" Producto: " + producto + " " + version);
		System.out.println(" Soporta el SQL ANSI92: " + soportaSQL);
		System.out.println(" Soporta la funcion CONVERT entre tipos SQL: " + soportaConvert);
		System.out.println(" Almacena las tablas en ficheros locales: " + usaFich);
		System.out.println(" Nombre del usuario conectado: " + nombre);
		System.out.println(" URL de la Base de Datos: " + url);
		System.out.println(" Soporta GROUP BY: " + soportaGRBY);
		System.out.println(" Soporta la minima gramatica SQL: " + soportaMinSQL);
		System.out.println(" Soporta transacciones: " + soportaTransac);
		System.out.println(" Soporta ejecución en batch: " + soportaBatch);
		System.out.println(" Soporta resultset sensible: " + soportaRsSensible);
		System.out.println(" Soporta resultset no sensible: " + soportaRsNoSensible);
		System.out.println(" Soporta resultset unidireccional: " + soportaRsForward);

		System.out.println(" ...");
		System.out.println();
	}

	private static void infoVersiones(DatabaseMetaData dbmd) throws SQLException {
		System.out.println(">>>Informacion sobre el driver:");
		String driver = dbmd.getDriverName();
		int jdbcMajorVersion = dbmd.getJDBCMajorVersion();
		int jdbcMinorVersion = dbmd.getJDBCMinorVersion();
		String driverVersion = dbmd.getDriverVersion();
		int verMayor = dbmd.getDriverMajorVersion();
		int verMenor = dbmd.getDriverMinorVersion();
		System.out.println("--------------------------- ");
		System.out.println(" JDBC Version: " + jdbcMajorVersion + "." + jdbcMinorVersion);
		System.out.println(" Driver: " + driver + " " + driverVersion);
		System.out.println(" Version superior del driver: " + verMayor);
		System.out.println(" Version inferior del driver: " + verMenor);
		System.out.println();
	}

	private static void infoFunciones(DatabaseMetaData dbmd) throws SQLException {
		System.out.println(">>>Funciones del DBMS:");
		String funcionesCadenas = dbmd.getStringFunctions();
		String funcionesSistema = dbmd.getSystemFunctions();
		String funcionesTiempo = dbmd.getTimeDateFunctions();
		String funcionesNumericas = dbmd.getNumericFunctions();
		System.out.println(" Funciones de Cadenas: " + funcionesCadenas);
		System.out.println(" Funciones Numericas: " + funcionesNumericas);
		System.out.println(" Funciones del Sistema: " + funcionesSistema);
		System.out.println(" Funciones de Fecha y Hora: " + funcionesTiempo);
		System.out.println();

	}

	private static void infoTablas(DatabaseMetaData dbmd) throws SQLException {
		System.out.println(">>>Tablas existentes:");
		String patron = "%"; // listamos todas las tablas
		String tipos[] = new String[1];
		tipos[0] = "TABLE"; // tablas de usuario
		// tipos[1] = "VIEW"; // vistas
//        tipos[2] = "SYSTEM TABLE";  //tablas del sistema
		ResultSet tablas = dbmd.getTables(null, "empresa_ad", patron, tipos);
		while (tablas.next()) {
			// Por cada tabla obtenemos su nombre y tipo
			System.out.println("Catálogo/bd: " + tablas.getString("TABLE_CAT") + " - Esquema: "
					+ tablas.getString("TABLE_SCHEM") + " - Tipo: " + tablas.getString("TABLE_TYPE") + " - Nombre: "
					+ tablas.getString("TABLE_NAME"));
		}
		System.out.println();

	}

	private static void infoProcedimientos(DatabaseMetaData dbmd) throws SQLException {
		if (dbmd.supportsStoredProcedures()) {
			System.out.println(">>>Procedimientos/Funciones almacenadas:");
			String patron = "%";
			ResultSet procedimientos = dbmd.getProcedures(null, "empresa_ad", patron);
			while (procedimientos.next()) {
				Short tipo = procedimientos.getShort("PROCEDURE_TYPE");
				System.out.print((tipo == 1) ? "Procedimiento " : "Función ");
				System.out.println(procedimientos.getString("PROCEDURE_NAME"));
			}
			ResultSet funciones = dbmd.getFunctions(null, "empresa_ad", patron);
			while (funciones.next()) {
				System.out.println("Función " + funciones.getString("FUNCTION_NAME"));
			}
		} else {
			System.out.println(">>>El DBMS no soporta procedimientos almacenados");
		}
		System.out.println();
	}

	private static void infoBD(DatabaseMetaData dbmd) throws SQLException {
		System.out.println(">>>Bases de datos disponibles:");
		ResultSet rs = dbmd.getCatalogs();
		while (rs.next()) {
			System.out.println(rs.getString("TABLE_CAT"));
		}
		System.out.println("-- Esquemas");
		rs = dbmd.getSchemas();
		while (rs.next()) {
			System.out.println(rs.getString("TABLE_SCHEM"));
		}
	}

	private static void verDatosTabla() {

		System.out.println(">>>Información tabla concreta:");
		System.out.println("Forma1: con getColumns");
		// Forma1
		try {
			ResultSet columnas = con.getMetaData().getColumns(null, null, "cliente", null);
			while (columnas.next()) {
				String columnName = columnas.getString("COLUMN_NAME");
				int columnSize = columnas.getInt("COLUMN_SIZE");
				int dataType = columnas.getInt("DATA_TYPE");
				String dataTypeName = JDBCType.valueOf(dataType).getName();
				String isNullable = columnas.getString("IS_NULLABLE");
				String isAutoIncrement = columnas.getString("IS_AUTOINCREMENT");
				System.out.println("Columna: " + columnName + " - Size: " + columnSize + " - Tipo: " + dataTypeName
						+ " - Acepta null: " + isNullable + " - Autoinc: " + isAutoIncrement);
			}
		} catch (SQLException ex) {
			System.err.println("Error" + ex.getMessage());
		}

		// Forma2
		System.out.println("Forma2: a través de una consulta concreta");
		try (Statement st = con.createStatement(); ResultSet rs = st.executeQuery("select * from cliente")) {
			// Obtiene metadatos de la consulta
			ResultSetMetaData rsmd = rs.getMetaData();

			// Muestra total de campos
			int numColumnas = rsmd.getColumnCount();
			System.out.println("Num. columnas: " + numColumnas);

			// Muestra información detallada de los campos
			for (int columna = 1; columna <= numColumnas; columna++) {
				System.out.println("Columna " + columna + " - Nombre: " + rsmd.getColumnName(columna)
						+ " - Tipo datos: " + rsmd.getColumnTypeName(columna) + "(" + rsmd.getColumnDisplaySize(columna)
						+ ")" + " Tam: " + rsmd.getPrecision(columna) + " " + rsmd.getScale(columna) + " Acepta null: "
						+ rsmd.isNullable(columna) + " AutoInc: " + rsmd.isAutoIncrement(columna));
			}

			// En la forma2, además, podemos mostrar los registros de la tabla formateados
			mostrarTablaRegistrosFormateados(rs);

		} catch (SQLException ex) {
			System.err.println("Error" + ex.getMessage());
		}

	}

	private static void mostrarTablaRegistrosFormateados(ResultSet rs) throws SQLException {
		// Muestra registros formateados
		// Cabecera
		ResultSetMetaData rsmd = rs.getMetaData();
		int numColumnas = rsmd.getColumnCount();

		for (int columna = 1; columna <= numColumnas; columna++) {
			String nombreColumna = rsmd.getColumnName(columna);
			int tamanyoVisualizacion = rsmd.getColumnDisplaySize(columna);
			if (nombreColumna.length() > tamanyoVisualizacion) {
				nombreColumna = nombreColumna.substring(0, tamanyoVisualizacion);
			}
			System.out.printf("%-" + tamanyoVisualizacion + "s\t", nombreColumna);
		}
		System.out.println();
		for (int columna = 1; columna <= numColumnas; columna++) {
			System.out.print("=".repeat(rsmd.getColumnDisplaySize(columna)) + "\t");
		}
		System.out.println();

		// Registros
		Object dato;
		while (rs.next()) {
			for (int columna = 1; columna <= numColumnas; columna++) {
				dato = rs.getObject(columna) != null ? rs.getObject(columna) : "";
				System.out.printf("%-" + rsmd.getColumnDisplaySize(columna) + "s\t", dato);
			}
			System.out.println();
		}

	}

	private static void ejercicio1() {
	}

	private static void ejercicio2() {

	}

}
